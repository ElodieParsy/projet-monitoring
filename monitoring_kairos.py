import os
import subprocess
import time
from datetime import datetime
import pymongo
from pymongo import MongoClient
import json


###########################################################
# On récupère le nombre de fichier statique présent (dev) #
###########################################################

commande = "cd /work/www/applications/kairos3.1/files && ls -l | wc -l"
resultat = subprocess.Popen(commande, stdout=subprocess.PIPE, shell=True)
files, error = resultat.communicate()
files = str(files.decode("utf-8"))

#On enlève les espaces autour du nombre de fichier
files = files.rstrip()

###########################################################
# On récupère le nombre de fichier statique présent (PRA) #
###########################################################

commande_PRA = "sudo ssh root@dsi-ope-lp02 'cd /work/www/applications/kairos3.1/files && ls -l | wc -l'"
resultat_commande = subprocess.Popen(commande_PRA, stdout=subprocess.PIPE, shell=True)
files_PRA, error = resultat_commande.communicate()
files_PRA = str(files_PRA.decode("utf-8"))

#On enlève les espaces autour du nombre de fichier
files_PRA = files_PRA.rstrip()

##############################################################################
# On récupère la date du dernier fichier statique modifié en timestamp (dev) #
##############################################################################

cmd = "cd /work/www/applications/kairos3.1/files && stat $(ls -rt | tail -1) --format=%Y" 
resultat_cmd = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
timestamp, error = resultat_cmd.communicate()
timestamp = str(timestamp.decode("utf-8"))

#On enlève les espaces autour de la date
timestamp = timestamp.rstrip()

###############################################################################################################
# On se place sur le serveur du PRA puis on récupère la date du dernier fichier statique modifié en timestamp #
###############################################################################################################

cmd_PRA = "sudo ssh root@dsi-ope-lp02 'cd /work/www/applications/kairos3.1/files && stat $(ls -rt | tail -1) --format=%Y'" 
resultat_cmd_PRA = subprocess.Popen(cmd_PRA, stdout=subprocess.PIPE, shell=True)
timestamp_PRA, error = resultat_cmd_PRA.communicate()
timestamp_PRA = str(timestamp_PRA.decode("utf-8"))

#On enlève les espaces autour du de la date
timestamp_PRA = timestamp_PRA.rstrip()

####################################################################
# On récupère la date du dernier fichier sur la base mongoDB (dev) #
####################################################################

connectionKairos3 = MongoClient("mongodb://127.0.0.1:27017") 
kairos3 = connectionKairos3['kairos-3-1'] 
nodes = kairos3.nodes

query = nodes.find({}, {"update.date": 1, "_id": 0}).sort([("update.date", pymongo.DESCENDING)]).limit(1) 
last_doc_prod = query[0]
ts_db_prod = last_doc_prod["update"]["date"]

####################################################################
# On récupère la date du dernier fichier sur la base mongoDB (PRA) #
####################################################################

connectionKairos3 = MongoClient("mongodb://dsi-ope-lp02:27017") 
kairos3 = connectionKairos3['kairos-3-1'] 
nodes = kairos3.nodes

query = nodes.find({}, {"update.date": 1, "_id": 0}).sort([("update.date", pymongo.DESCENDING)]).limit(1)  
last_doc_PRA = query[0]
ts_db_PRA = last_doc_PRA["update"]["date"]

##############################################################################################
# On compare la date du dernier fichier statique modifié du PRA à celui en DEV (D = PRA-DEV) #
# Si :                                                                                       #
# - D < 1200 , la sauvegarde est à jour avec un décalage inférieur à 30min int = 0           #
# - D < 4000, la sauvegarde est en cours avec un décalage inférieur à 130min int = 1         #
# - D > 4000, la sauvegarde est trop en retard il y a un problème int = 2                    # 
##############################################################################################

D = int(timestamp_PRA) - int(timestamp)
i = 0

if D > -1800 and D < 1800:
    i = 0

if D > -4000 and D < 4000:
    i = 1

if D < -4000 or D > 4000:
    i = 2

#############################################################################################################
# On compare la date du dernier fichier sur le serveur Mongo modifié du PRA à celui en DEV (Diff = PRA-DEV) #
# Si :                                                                                                      #
# - Diff < 1200 , la sauvegarde est à jour avec un décalage inférieur à 30min int = 0                       #
# - Diff < 4000, la sauvegarde est en cours avec un décalage inférieur à 130 min int = 1                    #
# - Diff > 4000, la sauvegarde est trop en retard il y a un problème int = 2                                # 
#############################################################################################################

D = ts_db_PRA - ts_db_prod
i2 = 0

if D > -1800 and D < 1800 :
    i2 = 0

if D > -4000 and D < 4000:
    i2 = 1

if D < -4000 or D > 4000:
    i2 = 2

#On créer un dictionnaire contenant nos données
data= {"nombre_fichier_dev":files, 
       "nombre_fichier_PRA":files_PRA,
       "last_file_dev":timestamp, 
       "last_file_PRA":timestamp_PRA,
       "last_Mongo_dev":ts_db_prod, 
       "last_Mongo_PRA":ts_db_PRA,
       "sync_stat_static":i,
       "sync_state_db":i2}

dataJ= json.dumps(data)
print(dataJ)